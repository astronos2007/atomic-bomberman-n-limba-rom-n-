10,menuexit

20,letter1
21,letter2

; un sunet ceva de genul "nu poti face asta aici"
40,enrt1
41,enrt2

; sunetele la punerea unei bombe
100,bmdrop2
101,bmdrop3
102,bmdrop1

; sunetele la lovirea unei bombe
120,kbomb1
121,kbomb2
122,kicker3
123,kicker10

; oprirea unei bombe
130,bombstop
131,bmbstop1
132,bmbstop2

; ricosarea bombei saltarete
135,bombboun
136,1017
137,1036

; 139 este ultimul sunet boing al jeleului

; o tigla masiva trantindu-se in locasul ei (dupa ce mesajul "hurry" (grabiti-va) este afisat)
; NOTA! codul este STRICT-CODIFICAT sa redea unul din cele trei sunete de mai jos in mod aleator.
; daca mai adaugi sunete sub 142, nu vor fi folosite!!!
140,clikplat
141,sqrdrop2
142,sqrdrop4
143,sqrdrop5
144,sqrdrop6
145,sqrdrop7
146,sqrdrop8

; pocnirea unei bombe
150,kbomb1
151,kbomb2

; o bomba pocnita/apucata saltand imprejur
160,bmdrop3

; cand apuci o bomba
170,grab1
171,grab2
172,bmbthrw1
173,bmbthrw3
174,bmbthrw4
175,bmbthrw5

; sunetele la explodarea bombelor
200,expl6
201,explo1
202,explode2
203,explode3
204,explode4
205,Bomb_01
206,Bomb_02
207,Bomb_04
208,Bomb_04b
209,Bomb_05
210,Bomb_06
211,Bomb_07
212,Bomb_07b
213,Bomb_09
214,Bomb_11
215,Bomb_12
216,Bomb_12b
217,Bomb_17
218,Bomb_19
219,Bomb_24
;299 este ultimul sunet la explodarea bombelor...

; sunetele atunci cand mori in flacari
300,scream1
301,die1

; avem un castigator al partidei
320,proud
321,theman
322,youwin1
323,jjfm2
324,jjfm8
325,jmb15
326,jmb6

; sunete ale animatiilor mortii BAZATE pe ce animatie este aleasa (sunt doar sunete de tip
; clicait care se sincronizeaza cu animatiile, nu sunt tipete sau altceva);
341,burnedup

; la saltul pe o trambulina
350,1017
351,1036
352,1045
353,trampo

; esti ametit de o bomba care aterizeaza pe capul tau
360,bombhit1
361,bombhit2
362,bombhit3
363,bombhit4

; obtii o putere speciala (una buna)
400,woohoo1
401,get1
402,get2
403,coolpop
404,ALLRITE
405,SCHWING
406,1006
407,1028
408,1041
409,1055
410,1059
411,1062
412,1074
413,myass1
414,hailgon
415,jjfm4a
416,jjfm4b
417,jmb1
418,jmb10
419,jmb14
420,jmb2
; 499 este ultimul sunet standard la "obtinerea unei puteri speciale"

; sunete diaree
550,poops1
551,poops2
552,poops3
553,poops4
554,poops5

; dupa moartea unui jucator
701,cribrown
702,cul8r
703,gotahurt
704,gotcha
705,later
706,roasted
707,toeasy
708,youblow
709,eatdust
710,smelsmok
711,stupidio
712,suckitdn
713,tastpain
714,tastpai2
715,hitstrid
716,jjfm5
717,jmb11
718,jmb16
719,jmb7
720,kisswook
; 999 este ultima batjocura posibila dupa moarte.

; muzica din pagina de titlu
1000,title
; muzica din meniul principal
1010,menu
; muzica pentru selectia perifericelor de intrare
1020,win
; muzica din ecranul de joc terminat
1030,lose
; te alaturi/initiezi unui/un joc in retea
1040,network
; muzica dintr-un joc propriu zis
1099,xxx			; doar pentru a preveni erorile
1100,grnacres
1101,generic
1102,hockey
1103,pyramid
1104,mineshft
1105,battle
1106,gieger
1107,haunted
1108,ocean
1109,swamp
1110,sewer

; ce redam cand jucam un joc in retea pe o instalare minimala.
1120,generic

; (folosita la incheierea unui joc, in general)
1130,draw

; dupa lasarea unui lant IMENS de bombe
1200,clear
1201,fireinh
1202,lookout
1203,litemup
1204,runaway1
1205,runaway2
1206,jjfm3
1207,jmb12
1208,jofm1
1209,jofm2
1210,godhere
;1299 este ultima valoare pentru sunetul "lant imens de bombe"

; sunetul pentru "ticaitul" periodic al ruletei
1300,1041

; oameni aplaudand, sunet fericit
1310,1000

; sunet de bazait, ai obtinut puterea speciala melasa de la ruleta (putere malicioasa)
1320,1049

; un sunet la transferul printr-o gaura-neagra
1330,warp1
1331,transin
1332,transout

; esti INCREDIBIL acum (a 7-a putere speciala si a 3-a ulterior)
1400,BACKUP
1401,BACKUP1
1402,BRINGON
1403,bringon
1404,COMEGET
1405,DABOMB
1406,ELAUGH1
1407,FEELPOWR
1408,OHYEAH
1409,PACKIN
1410,theman
1411,WHATRUSH
1412,WHODAD
1413,YEEHAW
1414,destu1
1415,destu2
1416,zen1
1417,zen2
1418,calldad
1419,drabom
1420,fillcrak
1421,inzone
1422,hitstrid
1423,inzone
1424,lovnit1
1425,luvyap
1426,playprop
1427,serendip
1428,shakeboo
1429,wantmnky
1430,wlktal1
1431,wlktal2
1432,wantmnky
1433,asskill
1434,brakhead
1435,bustass
1436,jjfm1
1437,jmb4
1438,jmb9
; 1699 este ultimul sunet pentru putere speciala INCREDIBILA...

; joc egal/remiza
1700,gump1
; 1999 este ultimul sunet pentru joc egal/remiza

; acestea sunt sunetele pentru "avem un castigator"... avem un castigator al partidei
2000,proud
2001,theman
2002,youwin1
; 2299 este ultimul numar al sunetului pentru "avem un castigator".

; sunet pentru o boala generica (nespecificata)
2300,PGAD12B
2301,beep
2302,fillcrak
; 2599 este ultimul sunet pentru o boala generica (nespecificata)

; iesire din joc (era si cazul... de-atata timp ma toci la cap aici asa!)
2600,quitgame
; 2699 este ultimul sunet pentru "iesirea din joc"

; se reda atunci cand mesajul "hurry" (grabiti-va) palpaie pe ecran.
2700,hurry
2701,hurytuf
; 2799 este ultimul sunet pentru "hurry up!" (grabiti-va)

; mesajul de inceput pe pagina de titlu "ATOMIC BOMBERMAN!" (cica asta-i joc fain!)
2800,GEN8A
; 2899 este ultimul sunet pentru mesajul de inceput "Atomic Bomberman!"

2900,0		;; valoare fictiva; nu o sterge!

; sunete specifica pentru fiecare boala in parte
; melasa
3000,disease1
3001,molases
3002,ohno1

; sfaramare
3050,disease2
3051,jmb8

; constipare
3100,disease3
;3101,DIS4A
;3102,DIS4B
;3103,DIS4C

; diaree
3150,DIS2A

; flama scurta
3200,disease1
3201,ewtahh

; sfaramare caca
3250,ewtahh

; fitil scurt
3300,disease2
3301,2mucpain

; interschimbarea a 2 jucatori
3350,disease3
3351,ohhhh

; controale inversate
3400,disease1
3401,jmb3

; lepra
3450,disease2
3451,jesus

; invizibil
3500,disease3

; bomba fara rost
3550,disease1
