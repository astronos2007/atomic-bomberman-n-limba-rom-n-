
;*****************************************************************************
;
; Autor:			Kurt W. Dekker
; Creat:			02/04/97 @ 12:32
; Actualizat:		07/11/97
; Numele fisierului:VALUELST.RES
;
; Descriere: O lista cu valorile temporizarilor, intarzierilor si numaratorilor.
;
; NOTA:  fiecare dintre aceste valori se gasesc in perechi: numarul resursei,
;		 valoare
;		 NU MODIFICA primul numar! Daca il modifici, programul se va opri
;		 deoarece nu poate gasi o anumita valoare.
;
; SANSE:  Daca nu sunt mentionate in alt mod, toate sansele (probabilitatile)
;		  sunt 1-la-N, unde actiunea va fi aleasa de 1 din N ori, unde N este
;		  numarul furnizat.
;
; VITEZELE: Daca nu sunt notate in alt mod, valorile postate pentru viteza
;			unui obiect sunt exprimate in sutimi de pixel per cadru. Asta
;			inseamna ca un obiect cu viteza 100 se va misca cu 1 pixel per
;			ciclu de 1 cadru.
;
;*****************************************************************************
;

; in cazul urmatoarelor doua valori (3 si 4), nu le setati pe amandoua la 1.
; setati doar una din ele la 1 la un moment dat.

; este activata preincarcarea SELECTIVA a sonorului? (doar intervale specifice
; de sunete)
; intervalele specifice incarcate ar afecta cel mai probabil dinamica jocului
; daca ar incarcate in timpul acestuia.
3,0

; este activata preincarcarea TOTALA a sonorului? (adica, preincarcam toate
; sunetele mai intai?). Pare sa fie o idee rea... mentine aceasta optiune la
; zero pentru moment.
; NOTA: aceasta incarca si sunetele nenecesare, de fapt incarca intregul fisier
; SOUNDLST.RES, deci ar putea cauza un deficit de performanta cand Windows
; incearca sa goleasca VM (memoria virtuala).
4,1

; cate efecte sonore diferite pot fi incarcate la un moment dat?
5,200	; numerele valide sunt intre 10 si 200
; cata memorie poate fi consumata de aceste sunete incarcate inainte de a incepe
; sa le inlaturam pe cele vechi pentru a elibera spatiu?
6,7000000	; numerele valide sunt intre 5mb si 20mb.

; cate seunde intre eliberarile voluntare ale memoriei ocupate de sunete si
; realegerea/reincarcarea fisierului SOUNDLST.RES?
; (daca pui un zero aici, memoria nu va fi eliberata voluntar niciodata.)
7,1800

; cate sunete concurente sunt permise?
8,5

; cat de des vor fi depurate sunetele incarcate (vezi #7 de mai sus) cand este
; activata urmarirea memoriei minime (de exemplu, cand ai mai putin de 32 mb de RAM).
; (timpul este in secunde)
9,100

; cate cadre ale animatiei pentru flama se redau la intervale regulate
10,10											; PGT

; numarul implicit de secunde in care ecranele principale vor astepta o apasare de tasta
12,7

; numarul MINIM de secunde de asteptare la ecrane pentru ca celelalte calculatoare
; sa tina pasul (nu afecteaza jocurile care nu sunt in retea)
13,3

; valoarea implicita pentru "repartizam un jucator pe tastatura?"
14,1

; este manualul de instructiuni activat?
15,1

; cate fisiere "glue" diferite sunt disponibile? (glue0, glue1, etc.)
; sunt denumite de la 0, deci daca ai glue0 si glue1, numarul va fi 2.
16,7

; cate cadre pentru animatia dezintegrarii caramizilor sunt redate la intervale regulate
20,10											; PGT

; cate cadre (rata standard) iti trebuie pentru a-l scoate din joc pe celalalt jucator?
; ** este doar o rata de cadre nominala care este folosita ca referinta pentru alte
;    valori bazate pe contoare de cadre din acest fisier! Nu modifica aceasta valoare
;    pentru ca nu iti va imbunatati performanta in niciun fel, si mai poate cauza si
;    alte lucruri sa se defecteze!
25,20											; PGT

; adancimea implicita a inchizaturii (cat de mult se va inchide terenul de joc)
; 0 inseamna deloc, 1 inseamna 2 randuri, 2 inseamna 4 randuri, 3 inseamna peste tot
27,1
28,4		; posibilitatile diferite de adancimi ale inchizaturii (0, 1, 2, 3 pentru moment)

; cate cadre pe secunda sa incercam sa obtinem?
30,20

; care este numarul maxim de milisecunde in care jocul poate avansa la orice "cadru" dat.
; aceasta previne o problema a discului care ii muta pe toti la o distanta imensa pe
; ecran, dand astfel lucrurile peste cap.
31,150											; PGT

; cate cadre "standard" afisam culorile reale inainte de a comuta la afisarea culorilor
; echipelor (doar in jocul pe echipe).
32,40

; cate nivele diferite (stagii) sunt definite?
; daca modifici acest numar si joci in retea, asigura-te ca ceilalti jucatori au EXACT
; aceleasi numere ale stagiilor diferite, precum si toate datele necesare pentru acele
; stagii!
35,11

; valoarea implicita pentru "repartizam aleatoriu pozitiile de inceput ale jucatorilor?
40,1

; cateva chestii de setat pentru tine:
; lungimea fitilului (cadre)
41,40											; PGT
; viteza implicita de inceput (sutimi de pixel/cadru)
42,923											; PGT
; tipul de bomba implicit
;	0 - bombe obisnuite
;	1 - bombe controlate (cu declansator)
;	2 - bombe saltarete
43,0											; PGT

; cand un segment din perete se inchide pe o bomba, bomba se dezamorseaza?
; 0 - distruge bomba, 1 - detoneaza bomba
; (este o valoare implicita; altfel, setarile nu o vor lua in seama)
46,1

; cate "Nume de cod" diferite sunt in fisierul messages.txt (#500)
47,49

; urmatoarele informatii reprezinta cu ce incep TOTI jucatorii la inceputul
; fiecarui nivel sau joc.
50,1		; numarul de bombe											; PGT
51,2		; lungimea flacarii (celule de dupa explozia din epicentru)	; PGT
52,0		; boli (aceasta nu e folosita!!)							; PGT
53,0		; lovituri													; PGT
54,0		; patine (puteri speciale pentru viteza)					; PGT
55,0		; pumni (manusi rosii)										; PGT
56,0		; apucari (manusi albastre)									; PGT
57,0		; multibombe												; PGT
58,0		; flacari aurii												; PGT
59,0		; bombe declansatoare										; PGT
60,0		; bombe saltarete											; PGT
61,0		; superboli (aceasta nu e folosita!!)						; PGT
62,0		; aleatorii (aceasta nu e folosita!!)						; PGT
63,0		; nefolosit inca											; PGT
64,0		; nefolosit inca											; PGT


; cata viteza in plus (sutimi de pixel/cadru) iti confera o patina?
90,150											; PGT
; cata viteza iti reduc FRANELE (puterea speciala malicioasa de la ruleta)
91,150											; PGT

; cate secunde asteptam inainte de a intra in modul "atragere"?
; daca pui un numar <5 aici, nu se va intra niciodata in modul atragere.
92,30

; care sunt sansele de a primi o batjocura dupa moarte ("floare la ureche")
95,5

; durata (in secunde) a unui meci implicit
100,150		; 150 este o valoare draguta

; timpul ramas dupa ce mesajul "hurry" (grabiti-va) clipeste pentru prima oara
; NU ESTE RECOMANDAT sa modifici aceasta valoare! Multe lucrri ciudate se pot
; intampla daca setezi valori "non standard". Daca il lasi la 60, o mare parte
; din program ar trebui sa fie in regula...
101,60											; PGT

; perioada de timp in care puterile speciale "super-puternice" nu vor aparea
; (vor aparea in alta parte)
102,40											; PGT

; cate animatii diferite pentru moarte avem? (de la die 1 pana la die 24)
; *** daca modifici asta si joci in retea, asigura-te ca ceilalti jucatori au
; EXACT acelasi numar al animatiilor pentru moarte!!!
105,24		;;; 24

; cati pixeli per referinta de cadru se misca in partea de sus animatia pentru
; moarte #9 (ingerul)?
106,4

; coordonatele pe ecran in care vor fi afisate valorile cronometrului
110,525
111,36
112,4		; pixeli spatiu in plus intre cifrele cronometrului

; doua coordonate verticale (Y) ale fiecarui rand cu jucatori deasupra partii de sus
113,6
114,26

; coordonatele din stanga (X) ale fiecarui rand cu jucatori deasupra partii de sus
115,10
116,110
117,210
118,310
119,410

; chestii referitoare la boli (setarile implicite)
; pot bolile fi distruse ca si celelalte puteri speciale?
; gbl_diseases_can_be_destroyed;
120,1											; PGT

; efectul unei boli "inceteaza" dupa un anumit timp?
; gbl_diseases_are_time_limited;
121,1											; PGT

; o boala va fi "reciclata" ca si alte puteri speciale cand iese din tine?
; gbl_diseases_will_recycle;
122,0											; PGT

; o boala se va multiplica cand e imprastiata, sau doar va fi "molipsita"?
; gbl_diseases_multiply;
123,1											; PGT

; obtinand o noua putere speciala (sau o alta boala), vei fi "vindecat" de boala precedenta?
; gbl_diseases_are_curable;
124,1											; PGT

; iata care sunt sansele ca o boala sa fie vindecata cand obtii o noua putere
; speciala (numerele mai mari reprezinta o sansa mai mica; este de 1 la N)
; gbl_diseases_cure_chance;
125,10											; PGT

; timpul minim de "detinere" a unei boli pana cand poate fi molipsita din nou (pentru a
; preveni molipsirea aleatorie de mai multe ori la un singur contact).  (disease_freshness)
129,10											; PGT

; cad de mult tine fiecare tip de boala (vezi DISEASE.H)
130,300											; PGT
131,300											; PGT
132,300											; PGT
133,300											; PGT
134,300											; PGT
135,300											; PGT
136,300											; PGT
137,300											; PGT
138,300											; PGT

; gata cu chestiile referitoare la boli

; trei viteze cu care banda transportoare se misca (sutimi de pixel)
189,3		; iata cate viteze diferite exista...
190,250		; mica								; PGT
191,350		; medie	    						; PGT
192,450		; mare  							; PGT

; valorile impicite pentru remaparea culorilor (perechi RGB, de la 0 la 9 din fisierele .RMP)
200,100				; 0.RMP: alb
201,100
202,100

205,20				; 1.RMP: negru
206,20
207,20

210,100				; 2.RMP: rosu
211,0
212,0

215,0				; 3.RMP: albastru
216,0
217,100

220,0				; 4.RMP: verde
221,100
222,0

225,100				; 5.RMP: galben
226,100
227,0

230,0				; 6.RMP: azuriu
231,100
232,100

235,100				; 7.RMP: purpuriu
236,0
237,100

240,100				; 8.RMP: portocaliu
241,50
242,0

245,50				; 9.RMP: mov
246,0
247,100

; cateva viteze ale bombelor...
300,1000	; cat de repede se misca o bomba lovita?  (1000 e dragut)		; PGT
301,1300	; cat de repede se misca o bomba pocnita? (1300 e dragut)		; PGT

; cate runde castigate pentru victorie? (implicit; se inlocuieste prin configurarea setarilor)
310,2

; numarul minim de secunde dintre bombele fara rost (dud) potentiale
320,180
; numar aleator aditional de secunde dintre bombele fara rost (dud) potentiale
321,180
; sansa unei bombe fara rost (1 in acest numar; numerele mai mari inseamna mai rar; nu folosi zero!
322,3
; cate cadre de animatie asteapta o bomba fara rost (minim)
323,120
; cate cadre de animatie asteapta o bomba fara rost (aleator aditional)
324,200

; cate animatii diferite pentru colturi (cornerhead) avem (0, 1, etc.)
; daca modifici acest numar, asigura-te ca toti ceilalti jucatori din retea
; au animatiile tale cornerhead noi si ca acest numar este EXACT acelasi pe
; toate calculatoarele!!
330,13		;;; 13

; care nivele au caramizi regeneratoare, si cate secunde intre incercarile de a
; regenera o caramida. (zero inseamna fara regenerare)
340,0
341,0
342,0
343,0
344,0
345,0
346,0
347,4	; cimitir/morga
348,0
349,0
350,0

; acestea reprezinta cate din fiecare tip de puteri speciale apar intr-un nivel
; un numar negativ indica de cate ori se va incerca intr-on sansa de 1-la-10
; pentru a amplasa o putere speciala.
400,10	; bombe
401,10	; flacari
402,3	; boli
403,4	; lovituri
404,8	; patine
405,2	; pumn
406,2	; apucare
407,1	; multibomba
408,-2	; flacara aurie
409,-4	; bombe declansatoare
410,1	; bombe saltarete
411,-4	; superboli
412,-2	; puteri speciale aleatorii

; acestea sunt valorile pentru "intarzierea de pe gheata" (cat de mult sunt
; incetinite controalele de prezenta ghetii in fiecare nivel). Acestea sunt
; masurate in milisecunde.
449,0		; doar pentru a preveni accesul invalid in jocurile in retea...
450,0		; nivel nou traditionalist clasic		; PGT
451,0		; clasic								; PGT
452,250		; ringul de hockey						; PGT
453,0		; egiptul antic							; PGT
454,0		; mina de carbune						; PGT
455,0		; plaja									; PGT
456,0		; extraterestri							; PGT
457,0		; casa bantuita							; PGT
458,0		; in adancul oceanului					; PGT
459,0		; padurea verde							; PGT
460,0		; gunoiul din oras						; PGT

; acesta este curba (in sus) unei bombe care este ridicata
500,12,10
502,25,20
504,25,30
506,12,40

; urmatoarele reprezinta LIMITELE pentru cate puteri speciale din fiecare poate
; un jucator acumula. Daca setezi prea mult, multe lucruri se pot strica...
; daca numarul este zero (0), nu exista limita pentru cate puteri poti acumula.

550,8		; numarul de bombe											; PGT
551,8		; lungimea flacarii (celule de dupa explozia din epicentru)	; PGT
552,0		; boli (aceasta nu e folosita!!)							; PGT
553,1		; lovituri													; PGT
554,4		; patine (puteri speciale pentru viteza)					; PGT
555,1		; pumni (manusi rosii)										; PGT
556,1		; apucari (manusi albastre)									; PGT
557,1		; multibombe												; PGT
558,1		; flacari aurii												; PGT
559,1		; bombe declansatoare										; PGT
560,1		; bombe saltarete											; PGT
561,0		; superboli (aceasta nu e folosita!!)						; PGT
562,0		; aleatorii (aceasta nu e folosita!!)						; PGT
563,0		; nefolosit inca											; PGT
564,0		; nefolosit inca											; PGT

; iata locatiile de inceput X,Y implicite pentru fiecare jucator
; (numerele negative se impacheteaza din coltul din dreapta / jos al ecranului)
600,0,0
602,-1,-1
604,0,-1
606,-1,0
608,6,4
610,8,0
612,-3,4
614,2,-5
616,-5,-3
618,6,-1

; pentru puterea speciala multibomba, aceasta determina sansa (1 la N) de redare
; a sunetelor "La o parte" sau "Da-le foc"
650,4
; si aceasta este ceea ce constituie bombele lasate "multe"
651,4

; cat de inalt este "arcul" facut de o bomba pocnita?
660,65		; ricoseuri initiale de 3 spatii ale pocniturii
661,20		; ricoesuri mici ulterioare de 1 spatiu ale pocniturii

665,2		; cat de mult "eziti" cand obtii o bomba?	; PGT

; la fiecare intersectie 0,0, care sunt sansele ca o bomba saltareata pocnita
; sa-si schimbe directia nebuneste?
667,3											; PGT

670,1		; care este numarul minim de puteri speciale pe care le pierzi cand esti lovit in cap?		; PGT
671,3		; care este numarul aditional aleator de puteri speciale pe care le poti pierde?			; PGT

; cate cadre te balansezi pe o trambulina?
680,30											; PGT
681,35		; cati pixeli te misti vertical in fiecare cadru?		; PGT

; rata de clipire a cursorului cu individul-bomber mititel (baza, aleator)
690,2,2

; raza "libera" a celulei care trebuie sa exista in jurul unui loc cu o caramida potential-
; regeneratoare. Nimeni nu poate sta in aceasta raza.
695,4

; urmatoarele reprezinta coordonatele pentru diversele ecrane din meniu
; sunt date drept X, Y, SY, si L, unde:
;		X, Y - primul (de sus) obiect din meniu
;		SY - spatierea-y in josul listei
;		L - latimea campului imprimat (daca este aplicabil)
; MENIUL PRINCIPAL:
; aceasta este lista punctelor selectabile din meniul principal:
700,332,140, 38,  0

; SELECTIA CONTROALELOR JUCATORULUI:
; textul adnotare al listelor cu tipul de control al jucatorului
705, 40,140,  0,200
; lista actuala al tipurilor de controale al jucatorului
710, 70,170, 24,150
; textul adnotare al joystick-urilor disponibile
715,300,140,  0,170
; lista actuala a joystick-urilor disponibile
720,320,170, 24,320

; ECRANUL CU OPTIUNI:
; unde blocurile "esantion" sunt afisare pe ecran
; X, Y, MarimeX, MarimeY (numar de blocuri)
730,400,100,5,5
; lista actuala a obiectelor optiuni
735, 55,170, 24,300

; ECRANUL CU SETARI:
; lista actuala a obiectelor optiuni
745, 55, 40, 22,500

; ECRANUL INTRARE IN RETEA:
; "Numele nostru de cod este:"
750,120, 80,  0,400
; "Jocuri in retea disponibile:
755,120,120,  0,250
; fiecare lista individuala de servere
760,120,140, 20,400

; ECRANUL INCEPE JOC IN RETEA:
; "Numele nostru de cod este:"
765,120, 80,  0,400
; "Jocuri in retea disponibile:
770,120,120,  0,250
; fiecare lista individuala de servere
775,120,140, 20,400

; ECRANUL CU CASTIGATORUL
; "Castigatorul a fost:"
780,150,140,  0,400
; fiecare jucator listat individual
785,150,210, 20,400

; "Apasa F1 pentru ajutor" (apare pe multe ecrane diferite)
790,320,440,  0,300

; "Continui tot cu acesti jucatori din retea??"
795, 80,150,  0,500

; Jucatorul %u a castigat partida...
800,150, 94,  0,400

; Ruleta Bombermanul de aur (titlul in partea de sus)
805,320, 30,  0,400

; Editorul - antetul din meniul principal
810, 50,100,  0,400
; Editorul - obiectele din meniul principal
815, 80,140, 20,400

; cate personalitati diferite ale COMPUTER-ILOR sunt predefinite?
900,1

; cat de departe semnifica zeul-foc (caramizile ce se inchid la final) un pericol
; pentru COMPUTERI?
910,15

; sansa ca un COMPUTER sa execute rutina "distruge caramizi"
915,5

; cat de aproape de un COMPUTER trebuie sa fie o putere speciala pentru a o lua?
920,4

; centrul ruletei pentru ecranul cu Bombermanul de aur
1000,320,240
; raza(ele) (x,y) ale perimetrului ruletei
1002,200,150
; rezolutia cercului activat de ruleta (300 este minim!)
1004,70
; parametrii X,Y for ruleta in forma de lissajous... (ranjet)
1006,1,1

; cate secunde dureaza sclipirea bombermanului de aur
; (zero il va face sa sclipeasca la nesfarsit)
1010,5

; cate milisecunde de asteptare inainte ca un pachet critic sa fie declarat intarziat
; si trebuie retrimis.
; aceasta valoare poate fi ajustaa in functie de latenta asteptata a retelei. Daca este
; setata prea mica, vei intampina multe retransmiteri nenecesare si performanta retelei
; va avea de suferit. Daca este setata prea mare, atunci intarzierea potentiala dintre
; jocuri (din cauza pachetelor ratate) poate deveni observabila si poate afecta
; integritatea/sincronizarea jocului.
1100,200		; revocare - fara joc in retea.
1101,200		; IPX
1102,750		; modem
1103,400		; serial
1104,400		; TCP/IP

; cate protocoale diferite (vezi lista de deasupra 1100) sunt suportate?
1110,4

; vectorul urmator activeaza/dezactiveaza diferite nivele ale selectiei JOC ALEATOR
; (pentru a putea sa blochezi niste nivele pe care nu vrei se la joci NICIODATA).
1150,1
1151,1
1152,1		; prea enervant sa ai intarzieri ale controalelor!
1153,1
1154,1
1155,1
1156,1		; prea greu de observat vizual!
1157,1
1158,1
1159,1
1160,1
1161,1

; chestii legate de modul campanie
; sansa ca o fantoma sau un hoinar isi va schimba directia la o intersectie
1200,3
; sansa ca schimbarea de directie sa NU fie inspre un jucator
1205,3

; puncte acordate pentru uciderea unui COMPUTER (doar in modul campanie!)
1300,250
; puncte acordate pentru uciderea unui hoinar (doar in modul campanie!)points for killing a rover (in campaign mode only)
1310,15
; puncte acordate pentru uciderea unei fantome (doar in modul campanie!)
1320,25
