
;*****************************************************************************
;
; Autor:		Kurt W. Dekker
; Creat:		03/11/97 @ 13:47
; Actualizat:	03/11/97
; Nume fisier:	EXTRA2.RES
;
; Descriere:	Fisier resursa pentru datele suplimentare ale unui nivel dat.
;
;*****************************************************************************
;

-A,S, 2, 2
-A,W,-3, 2
-A,N,-3,-3
-A,E, 2,-3

-A,E, 4, 4
-A,S,-5, 4
-A,W,-5,-5
-A,N, 4,-5

-A,E, 6,-1
-A,S, 0, 4
-A,W, 8, 0
-A,N,-1, 6
