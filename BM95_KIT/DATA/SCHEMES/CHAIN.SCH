
; NOTE! This is an Atomic Bomberman Scheme File.
; Modify at your own risk.  It is machine-generated and updated.


; this is an internal version control number
-V,2

; this is the textual name of the scheme
-N,LEGATURI IN LANT (4)

; scheme brick density (0-100 percent)
-B,100

; actual array data (# is solid, : is brick, . is blank)
;               11111
;     012345678901234
-R, 0,:::::::::::::::
-R, 1,:::::::::::::::
-R, 2,:::::::::::::::
-R, 3,#:#:#:#:#:#:#:#
-R, 4,...#...#...#...
-R, 5,...............
-R, 6,...#...#...#...
-R, 7,#:#:#:#:#:#:#:#
-R, 8,:::::::::::::::
-R, 9,:::::::::::::::
-R,10,:::::::::::::::

; player starting locations (playerno,X,Y)
-S,0,1,5,0
-S,1,5,5,1
-S,2,9,5,0
-S,3,13,5,1
-S,4,1,5,0
-S,5,5,5,1
-S,6,9,5,0
-S,7,13,5,1
-S,8,5,5,0
-S,9,9,5,1

; powerup information; the fields are:
;   powerup #, bornwith, has_override, override_value, forbidden
;   (note the last text field has no effect; it is only a comment)
-P, 0, 0,0, 0, 0,o bomba in plus
-P, 1, 0,0, 0, 0,flacara mai lunga
-P, 2, 0,0, 0, 0,o boala
-P, 3, 0,0, 0, 0,abilitatea de a lovi bombele
-P, 4, 0,0, 0, 0,extra viteza
-P, 5, 0,0, 0, 0,abilitatea de a pocni bombele
-P, 6, 0,0, 0, 0,abilitatea de a apuca bombele
-P, 7, 0,0, 0, 0,multibomba
-P, 8, 0,0, 0, 0,flacara de aur
-P, 9, 0,0, 0, 0,un mecanism de declansare
-P,10, 0,0, 0, 0,bombe de jeleu (saritoare)
-P,11, 0,0, 0, 0,super boala
-P,12, 0,0, 0, 0,aleator
